## For People Managers

1. [ ] People Experience: Ask the People Business Partner and the offboarded team member's Manager in the comments, who the new manager will be for the offboarded team members direct reports. 
This information is needed to update the Team Page and BambooHR. 
