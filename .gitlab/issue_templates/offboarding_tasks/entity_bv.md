### For GitLab BV employees only

<summary>People Experience</summary>

1. [ ] People Experience: Email HRSavvy with the former team members final dates of employment, inform co-employer payrolls.
1. [ ] People Experience: Ping Non-US Payroll (@hdevlin @nprecilla) 
