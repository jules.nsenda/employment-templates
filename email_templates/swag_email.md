Hi `__FIRST_NAME__`, 

Congrats on becoming a GitLab Team Member! We are so excited to have you joining our team, as a new team member we would like to share some GitLab swag with you!  Follow the directions below to get started. 

1. Please go to the <a href='https://shop.gitlab.com/'>GitLab Swag Store</a>
2. Choose some goods for yourself!
3. At checkout, use the code `__SWAG_CODE__` which will take $25USD off your order.
4. Shipping is free!
5. Please remember to use your GitLab email at checkout on the contact information page.

If your chosen item or size is out of stock, please keep checking back, as your code does not expire.

Please let me know if you have any questions!

Best regards,

People Experience Team
